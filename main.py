import os
import shutil
import binascii
import base64
import uuid
import zlib
import json 

from flask import Flask, jsonify
from flask import request
from process_elements import process_elements


app = Flask(__name__)

''' Delete later !'''
@app.route('/')
def index():
    return 'Index Page'


''' Png diagram image endpoint '''
@app.route('/diagrams', methods=['POST'])
def get_png():
    data = request.get_json()
    if data['uml'] == '':
        return jsonify({
	                        "error": {
	                        	"message": "No uml diagram found !",
	                        	"code": "202"
	                        }
                        }), 202
    if data['type'] == 'svg':
        return convert_data_text(data, type = True, hex = False)
    else:
        return convert_data_text(data, type = False, hex = False)



''' PlantUML online service URL link '''
@app.route('/image-links', methods=['POST'])
def get_hex_link_png():
    data = request.get_json()
    if data['uml'] == '':
        return jsonify({
	               "error": {
	               	"message": "No uml diagram found !",
	               	"code": "202"
	               }
               }), 202
    if data['type'] == 'svg':
        return convert_data_text(data, type = True, hex = True)
    else:
        return convert_data_text(data, type = False, hex = True)




''' Writes to a markdown file the given diagram '''
def convert_data_text(data, type, hex):
    #  test = open('test.md', 'rb')
    #  test_encode = base64.b64encode(test.read())
    #  test_encode = test_encode.decode('UTF-8')
    #  print("POSTMAN ", test_encode)
    file = open('plant_diagram.md', 'wb')
    file.write(base64.b64decode(data['uml']))
    file.close()
    set_names()
    if hex == False:
        return process_data(type)
    else:
        return data_to_hex(type)



''' Calls process elements for treqs, generates new file called gen and generates png/svg diagram image '''
def process_data(type):
    
    process = process_elements()
    process.process_plantuml_file('plant_diagram.md', type)
    diagram_names = get_name()
    return encode_diagram(diagram_names, type)
    #  clean_up()

''' Set new generated names for untitled diagrams'''
def set_names():
    file = open('plant_diagram.md', 'r')
    line_list = file.readlines()
    for index, line in enumerate(line_list):
        if line == '@startuml\n':
            diagram_id = uuid.uuid4()
            line_list[index] = '@startuml ' + 'plant_diagram{}{}'.format(str(diagram_id)[:5], '\n')
    file.close()
    w_file = open('plant_diagram.md', 'w')
    w_file.writelines(line_list)
    w_file.close()


''' Get names of each diagram from the uml code'''
def get_name():
    name_list = []
    file = open('plant_diagram.md', 'r')
    for line in file:
        if line.startswith("@start"):
            try:
                name = line.split(" ", 1)[1].replace("\n","")
                app.logger.info('Filename: %s', name)
                name_list.append(name)
            except IndexError:
                app.logger.info('Untitled plantUML diagram')
                app.logger.info('Name given to diagram --> plant_diagram')
                diagram_id = uuid.uuid4()
                name = 'plant_diagram' + '_{}'.format(str(diagram_id)[:4])
                name_list.append(name)
    file.close()

    return name_list


'''Encodes the png/svg diagram image, the user decodes the encoded string and saves the image on the users end'''
def encode_diagram(diagram_names, type):
    encode_dict = {}
    if type == False:
         for name in diagram_names:
            file = open('gen/' + name + '.png', "rb")
            encoded_diagram = base64.b64encode(file.read())
            encode_dict[name] = encoded_diagram.decode('UTF-8')
            file.close()
    else:
        for name in diagram_names:
            file = open('gen/' + name + '.svg', "rb")
            encoded_diagram = base64.b64encode(file.read())
            encode_dict[name] = encoded_diagram.decode('UTF-8')
            file.close()
    
    #'''Decode on the user end'''
    # with open("imageToSave.png", "wb") as fh:
    #     fh.write(base64.b64decode(encode_dict['Graph']))
    # zlib.compress(encoded_diagram)  Compression takes 1s longer time for a standard size diagram
    # encode_json = json.dumps(encode_dict)
    return jsonify(encode_dict), 201
   

''' Reads the markdown file as bytes and changes each ascii to hex so it can be used directly for the plantuml online service'''
def data_to_hex(type):
    uml_list = []
    get_names = get_name()
    text_file = open('plant_diagram.md', 'rb')
    txt_data = text_file.read()
    text_file.close()

    for uml in txt_data.split(b'*'):
        x = binascii.hexlify(uml)
        uml_list.append(x.decode('ascii'))

    return return_url(uml_list, type, get_names)


''' Creates a legal plantuml url for the user, direct link to a svg/png diagram image '''
def return_url(uml_list, type, name_list):
    url_dict = {}
    zip_lists = zip(name_list, uml_list)

    if type == False:
        for pair in list(zip_lists):
            url_dict[pair[0]] = "http://www.plantuml.com/plantuml/png/~h" + pair[1]
    else:
        for pair in list(zip_lists):
            url_dict[pair[0]] = "http://www.plantuml.com/plantuml/svg/~h" + pair[1]
    return jsonify(url_dict), 201
    

''' This is a clean up function meant to clean up space after user generates diagrams ''' 
def clean_up():
    if os.path.exists('plant_diagram.md'):
        # os.remove('plant_diagram.md')
        app.logger.info('Do no need to delete this !')
    if os.path.exists('gen'):
        shutil.rmtree('gen')
        app.logger.info('Gen folder deleted !')

    else:
        app.logger.info('No clean up needed !')


if __name__ == '__main__':
    app.run(debug=True)

