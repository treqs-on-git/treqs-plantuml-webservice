import json
import unittest
import os
from main import app


class FlaskTest(unittest.TestCase):

    def setUp(self):
        self.cwd = os.getcwd()
        self.tester = app.test_client(self)
        self.uml_data_with_name = {"type": "png","uml": "QHN0YXJ0dW1sIG1pbGFubw0KQWxpY2UtPkJvYiA6IEkgYW0gdXNpbmcgaGV4DQpAZW5kdW1s"}
        self.multiple_diagrams = {"type": "png","uml": "QHN0YXJ0dW1sIG1pbGFubwpBbGljZS0+Qm9iIDogbWlsYW5vCkBlbmR1bWwKKgpAc3RhcnR1bWwgaWNlbGFuZApBbGljZS0+Qm9iIDogdGVzdDEKQGVuZHVtbAoqCkBzdGFydHVtbCB0ZV9wb3QKQWxpY2UtPkJvYiA6IG5vdCBjb2ZmZWUKQGVuZHVtbAoqCkBzdGFydHVtbCBJZF9jaGVjawpBbGljZS0+Qm9iIDogaWRfY2hlY2sKQGVuZHVtbAo="}
        self.uml_data_with_name_svg = {"type": "svg","uml": "QHN0YXJ0dW1sIG1pbGFubw0KQWxpY2UtPkJvYiA6IEkgYW0gdXNpbmcgaGV4DQpAZW5kdW1s"}
        self.multiple_diagrams_svg = {"type": "svg","uml": "QHN0YXJ0dW1sIG1pbGFubwpBbGljZS0+Qm9iIDogbWlsYW5vCkBlbmR1bWwKKgpAc3RhcnR1bWwgaWNlbGFuZApBbGljZS0+Qm9iIDogdGVzdDEKQGVuZHVtbAoqCkBzdGFydHVtbCB0ZV9wb3QKQWxpY2UtPkJvYiA6IG5vdCBjb2ZmZWUKQGVuZHVtbAoqCkBzdGFydHVtbCBJZF9jaGVjawpBbGljZS0+Qm9iIDogaWRfY2hlY2sKQGVuZHVtbAo="}
        self.empty_json_msg = {"error": {"message": "No uml diagram found !","code": "202"}}
        self.valid_hex_url = {"milano": "http://www.plantuml.com/plantuml/png/~h407374617274756d6c206d696c616e6f0a416c6963652d3e426f62203a204920616d207573696e67206865780a40656e64756d6c"}
        self.multiple_hex_url = {"Id_check": "http://www.plantuml.com/plantuml/png/~h0a407374617274756d6c2049645f636865636b0a416c6963652d3e426f62203a2069645f636865636b0a40656e64756d6c0a","iceland": "http://www.plantuml.com/plantuml/png/~h0a407374617274756d6c206963656c616e640a416c6963652d3e426f62203a2074657374310a40656e64756d6c0a","milano": "http://www.plantuml.com/plantuml/png/~h407374617274756d6c206d696c616e6f0a416c6963652d3e426f62203a206d696c616e6f0a40656e64756d6c0a","te_pot": "http://www.plantuml.com/plantuml/png/~h0a407374617274756d6c2074655f706f740a416c6963652d3e426f62203a206e6f7420636f666665650a40656e64756d6c0a"}

    ''' Test for response 202 '''

    def test_png(self):
        response = self.tester.post('/diagrams', data=json.dumps({"type": "png","uml": ""}), content_type='application/json')
        status_code = response.status_code
        self.assertEqual(status_code, 202)

    def test_svg(self):
        response = self.tester.post('/diagrams', data=json.dumps({"type": "svg","uml": ""}), content_type='application/json')
        status_code = response.status_code
        self.assertEqual(status_code, 202)

    def test_url_png(self):
        response = self.tester.post('/image-links', data=json.dumps({"type": "png","uml": ""}), content_type='application/json')
        status_code = response.status_code
        self.assertEqual(status_code, 202)

    def test_url_svg(self):
        response = self.tester.post('/image-links', data=json.dumps({"type": "svg","uml": ""}), content_type='application/json')
        status_code = response.status_code
        self.assertEqual(status_code, 202)

    ''' Test endpoints that do not exist'''
    def test_status_404(self):
        response = self.tester.post('/diagrams/html', data=json.dumps({"type": "png","uml": ""}), content_type='application/json')
        status_code = response.status_code
        self.assertEqual(status_code, 404)  
       
    ''' Test if we get empty data '''    
    def test_png_empty_data(self):
         response = self.tester.post('/diagrams', data=json.dumps({"type": "png","uml": ""}), content_type='application/json')
         self.assertEqual(response.get_json() , self.empty_json_msg)

    ''' Test if url is valid and we converted the ascii string to HEX '''
    def test_png_hex_url(self):
         response = self.tester.post('/image-links', data= json.dumps(self.uml_data_with_name ), content_type = 'application/json')
         self.assertEqual(response.get_json(), self.valid_hex_url)


    ''' Test png generate diagram endpoint, diagram with a name '''
    def test_png_diagram_exist_name(self):
        self.tester.post('/diagrams', data= json.dumps(self.uml_data_with_name), content_type = 'application/json')
        self.assertEqual(os.path.exists(self.cwd + '/gen/milano.png'), True)
        self.assertEqual(os.path.exists(self.cwd + '/gen/milano.svg'), False)

    ''' Test svg generate diagram endpoint, diagram with a name '''
    def test_svg_diagram_exist_name(self):
        self.tester.post('/diagrams', data= json.dumps(self.uml_data_with_name_svg), content_type = 'application/json')
        self.assertEqual(os.path.exists(self.cwd + '/gen/milano.svg'), True)
        self.assertEqual(os.path.exists(self.cwd + '/gen/milano.png'), False)

    ''' Test the encoded png diagram endpoint '''
    def test_png_encoding_string(self):
        file = open('tests/encode_string_png.txt', 'r')
        encode_string = file.read()
        file.close()
        wbs_encode = self.tester.post('/diagrams', data= json.dumps(self.uml_data_with_name), content_type = 'application/json')
        self.assertEqual(wbs_encode.get_json()['milano'], encode_string)

    ''' Test the encoded svg diagram endpoint '''
    def test_svg_encoding_string(self):
        file = open('tests/encode_string_svg.txt', 'r')
        encode_string = file.read()
        file.close()
        wbs_encode = self.tester.post('/diagrams', data= json.dumps(self.uml_data_with_name_svg), content_type = 'application/json')
        self.assertEqual(wbs_encode.get_json()['milano'], encode_string)

    ''' Testing requests with multible diagrams '''
    def test_multiple_diagrams_png(self):
        self.tester.post('/diagrams', data= json.dumps(self.multiple_diagrams), content_type = 'application/json')
        self.assertEqual(os.path.exists(self.cwd + '/gen/milano.png'), True)
        self.assertEqual(os.path.exists(self.cwd + '/gen/iceland.png'), True)
        self.assertEqual(os.path.exists(self.cwd + '/gen/te_pot.png'), True)
        self.assertEqual(os.path.exists(self.cwd + '/gen/Id_check.png'), True)

    def test_multiple_image_links_png(self):
        multiple_hex_url_json = self.tester.post('/image-links', data= json.dumps(self.multiple_diagrams), content_type = 'application/json')
        self.assertEqual(multiple_hex_url_json.get_json(), self.multiple_hex_url)




    def tearDown(self):
        if os.path.exists(os.getcwd() + '/gen/plant_diagram.png'):
            os.remove(os.getcwd() + '/gen/plant_diagram.png')
        if os.path.exists(os.getcwd() + '/gen/milano.png'):
            os.remove(os.getcwd() + '/gen/milano.png')
        if os.path.exists(os.getcwd() + '/gen/milano.svg'):
            os.remove(os.getcwd() + '/gen/milano.svg')
        if os.path.exists(os.getcwd() + '/gen/plant_diagram.svg'):
            os.remove(os.getcwd() + '/gen/plant_diagram.svg')
        if os.path.exists(os.getcwd() + '/gen/iceland.png'):
            os.remove(os.getcwd() + '/gen/iceland.png') 
        if os.path.exists(os.getcwd() + '/gen/te_pot.png'):
            os.remove(os.getcwd() + '/gen/te_pot.png') 
        if os.path.exists(os.getcwd() + '/gen/Id_check.png'):
            os.remove(os.getcwd() + '/gen/Id_check.png')                        
        if os.path.exists(os.getcwd() + '/gen'):
            os.rmdir(os.getcwd() + '/gen')



if __name__ == '__main__':
    unittest.main()
