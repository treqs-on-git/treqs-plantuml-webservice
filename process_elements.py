import os
import re
import logging

#  from file_traverser import file_traverser


class process_elements:

    def __init__(self):
        self.logger = logging.getLogger('treqs-on-git.treqs-ng')
        #  self.traverser = file_traverser()
        self.logger.info('process_elements created')
        self.html_generation = False

    def process_plantuml(self, file_name, recursive, html = False):
        self.html_generation = html
        self.logger.info('process_plantuml started')
        self.traverser.traverse_file_hierarchy(file_name, recursive, self.process_plantuml_file)

    def process_plantuml_file(self, filename, type):
        self.make_plantuml_figures(filename, type)
        #  self.generate_markdown_file(filename)
        
    def make_plantuml_figures(self, filename, type):
        ''' Go through the file and create SVG/PNG files for each plantuml diagram. 
            Future versions may rely on online services, so that no dependency to Java is needed. 
            It is for example possible to create the figures on the fly via a webservice, so that 
            this method does not have to do anything. '''
        self.create_gen_dir()
        libdir = os.path.split(__file__)[0] + "/lib/plantuml.jar"
        self.logger.info('   Using plantuml binary at %s', libdir)
        if type:  
            os.system('java -jar %s -tsvg -o %s %s' % (libdir, os.getcwd() + '/gen/', filename))
        else:
            os.system('java -jar %s -png -o %s %s' % (libdir, os.getcwd() + '/gen/', filename))


    def create_gen_dir(self):
        ''' Creates a /gen directory if not exists. '''
        path = os.getcwd() + '/gen'
        if not os.path.exists(path):
            try:
                os.mkdir(path)
            except OSError:
                self.logger.error('Failed in creating /gen directory at %s' % path)
            else:
                self.logger.info('Creating /gen directory at %s' % path)



    def create_svg_string(self, filename):
        ''' Reads a given svg file and returns the contents in string format. '''
        with open(os.getcwd() + '/gen/' + filename + '.svg', 'r') as s:
            svg_string = s.readlines()[0]
            formated_svg_string = re.sub('\<!--MD5=.*$', '</g></svg>', svg_string)
            # self.logger.info(os.path.dirname(os.path.abspath(__file__)))
        return formated_svg_string

    
if __name__ == "__main__":
    pe = process_elements()
    # pe.extract_plant_uml("../requirements/4-process-elements.md")
