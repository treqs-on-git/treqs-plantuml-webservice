# T-reqs web service

The T-reqs web service handles UML/dot code request from the user and generates a diagram image png/SVG for the user. The web service can also create a direct URL link to the diagram.

## Installation

#### Create the environment
The first step is to create a virtual environment for the project from the root directory. 

##### macOS/Linux
```
$ cd treqs-plantuml-webservice
$ python3 -m venv venv
```
##### Windows
```
> cd myproject
> py -3 -m venv venv
```

### Activate the environment
Before you start the webservice you have to activate the environment.

##### macOS/Linux
```
$ . venv/bin/activate
```
##### Windows
```
> venv\Scripts\activate
```
### Install Flask
Within the activated environment install flask.
```
$ pip install Flask
```
If you do not have [pip](https://pypi.org/project/pip/), here are the install instructions.


### Before running
We have to set the exporting FLASK_APP environment variable:

##### Bash
```
$ export FLASK_APP=main
```
##### CMD
```
> set FLASK_APP=main
```
##### Powershell
```
> $env:FLASK_APP = "main"
```

### How to run

Now you can run the web service from your terminal:

```
$ flask run
```

## How to run tests

To run the unit tests you have to be at the root of the project and run this command in your terminal.


##### Bash
```
$ python3 -m unittest tests.test_main
```
##### Powershell
```
> python -m unittest tests.test_main
```

